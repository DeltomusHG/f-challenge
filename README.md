# Documentación para el challenge de backend developer
*La presente documentación omite partes de la api que se pueden explorar a través de la colección de postman de el siquiente link:
https://www.getpostman.com/collections/ecbf4a0fd29e3be19763

## Sobre cosas importantes en la base de datos:
La base de datos contiene catálogos necesarios para algunas operaciones, a continuación se recopilan los valores por defecto para poder usar la API:

**user_type**:
- 1 - Admin
- 2 - Event Organizer
- 3 - Assistant


**ticket_status**:
- 1 - Payed
- 2 - Cancelled
- 3 - Waiting list

**meet_type**:
- 1 - face-to-face
- 2 - virtual

**category_id**:
- 1 - Tech
- 2 - Bussiness
- 3 - Status Quo
- 4 - Videogames
- 5 - LGBTQ

## Token

### Get new Token

**URL** : `/api/token/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "email": "[valid email address]",
    "password": "[password in plain text]"
}
```

**Data example**

```json
{
    "email": "iloveauth@example.com",
    "password": "abcd1234"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU4Njg5NTUyMywianRpIjoiYjkwNzdhYTI2MmE2NDBmM2E5MDdjNjVjNWZlZmUzM2UiLCJ1c2VyX2lkIjoxfQ.r85zlF9dMv9Rs1c0e5U7vbBm_Xx4rSnpSwCRGUxos6k",
    "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTg2ODA5NDIzLCJqdGkiOiJkYjFmNTFkN2ExMmQ0M2IwYmM5OTY5OWViYjM1OWVlZSIsInVzZXJfaWQiOjF9.1GrjdVplMyh9DR70DFz9X0eXOHWWWX8w3UrGxEuSShU"
}
```

## Error Response

**Condition** : If 'username' and 'password' combination is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "No active account found with the given credentials"
}
```
***
### Refresh Token

**URL** : `/api/token/refresh/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "refresh": "[valid refresh token]"
}
```

**Data example**

```json
{
    "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU4Njg5NTY0MSwianRpIjoiMGExN2UwOGFiOWU0NDE4MWFhMWJiNmY2ZTBmNjU2NDciLCJ1c2VyX2lkIjoxfQ.mRJctc_5ajdBuVrMis-T32BmZ-zZpqJChptxstS6hh8"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTg2ODA5NDIzLCJqdGkiOiJkYjFmNTFkN2ExMmQ0M2IwYmM5OTY5OWViYjM1OWVlZSIsInVzZXJfaWQiOjF9.1GrjdVplMyh9DR70DFz9X0eXOHWWWX8w3UrGxEuSShU"
}
```

## Error Response

**Condition** : If 'username' and 'password' combination is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "Token is invalid or expired",
    "code": "token_not_valid"
}
```
***

### Location

### Create Location

**URL** : `/api/locations/`

**Method** : `POST`

**Auth required** : YES


**Data example**

```json
{
	"name":"Jardín de eventos sociales Francés",
	"country":"México",
	"state":"CDMX",
	"city":"Alcaldía Miguel Hidalgo",
	"neighboorhood":"Chapultepec V Sección",
	"street_name":"Montes Urales",
	"number":424,
	"zip_code":11000,
	"capacity":250
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU4Njg5NTUyMywianRpIjoiYjkwNzdhYTI2MmE2NDBmM2E5MDdjNjVjNWZlZmUzM2UiLCJ1c2VyX2lkIjoxfQ.r85zlF9dMv9Rs1c0e5U7vbBm_Xx4rSnpSwCRGUxos6k",
    "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTg2ODA5NDIzLCJqdGkiOiJkYjFmNTFkN2ExMmQ0M2IwYmM5OTY5OWViYjM1OWVlZSIsInVzZXJfaWQiOjF9.1GrjdVplMyh9DR70DFz9X0eXOHWWWX8w3UrGxEuSShU"
}
```

## Error Response

**Condition** : If 'username' and 'password' combination is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "name": [
        "Este campo es requerido."
    ],
    "country": [
        "Este campo es requerido."
    ],
    "state": [
        "Este campo es requerido."
    ],
    "city": [
        "Este campo es requerido."
    ],
    "neighboorhood": [
        "Este campo es requerido."
    ],
    "street_name": [
        "Este campo es requerido."
    ],
    "number": [
        "Este campo es requerido."
    ],
    "zip_code": [
        "Este campo es requerido."
    ],
    "capacity": [
        "Este campo es requerido."
    ]
}
```
***

### Meetings
### Create Meeting

**URL** : `/api/meetings/`

**Method** : `POST`

**Auth required** : YES


**Data example**

```json
{
	"name":"Reunión de motociclistas",
    "start_date": "2020-01-02T09:01:00",
    "end_date": "2020-01-02T11:30:00",
    "available_tickets": 50,
    "is_active": true,
    "waiting_list_tickets": 0,
    "location": 2,
    "meet_type": 1,
    "organizer": 1,
    "category": 1
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id": 17,
    "name": "Reunión de motociclistas",
    "start_date": "2020-01-05T09:01:00Z",
    "end_date": "2020-01-05T11:30:00Z",
    "available_tickets": 50,
    "is_active": true,
    "waiting_list_tickets": 0,
    "location": 2,
    "meet_type": 1,
    "organizer": 1,
    "category": 1
}
```

## Error Response

**Condition** : start_date or end_date cross with other event.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "start_date": [
        "Ya existe un/a meetings con este/a start date."
    ],
    "end_date": [
        "Ya existe un/a meetings con este/a end date."
    ]
}
```


```json
[
    "La fecha elegida se traslapa con otro evento"
]
```

***



### Users

### Create User

**URL** : `/api/users/`

**Method** : `POST`

**Auth required** : YES


**Data example**

```json
{
	"name": "Admin 2",
	"paternal_surname": "paternal",
	"maternal_surname": "maternal",
	"email": "admin2@meetdown.com",
	"username": "Admin_2",
	"password": "password",
	"user_type": 1
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id": 3,
    "last_login": null,
    "is_superuser": false,
    "username": "Admin_21",
    "first_name": "",
    "last_name": "",
    "is_staff": false,
    "is_active": true,
    "date_joined": "2020-04-13T20:40:16.791058Z",
    "name": "Admin 2",
    "paternal_surname": "paternal",
    "maternal_surname": "maternal",
    "email": "admin3@meetdown.com",
    "user_type": 1,
    "groups": [],
    "user_permissions": []
}
```

## Error Response

**Condition** : username or email already exists.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "username": [
        "Ya existe un usuario con ese nombre."
    ],
    "email": [
        "Ya existe un/a usuario con este/a email."
    ]
}
```


```json
[
    "La fecha elegida se traslapa con otro evento"
]
```

***

### Tickets

### Create ticket

**URL** : `/api/tickets/`

**Method** : `POST`

**Auth required** : YES


**Data example**

```json
{
	"user":1,
	"meeting":13,
	"location":2,
	"ticket_status":1
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id": 6,
    "ticket_status": 2,
    "user": 1,
    "meeting": 13,
    "location": 2
}
```

## Error Response

**Condition** : any id doesn't exists.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "location": [
        "Clave primaria \"12\" inválida - objeto no existe."
    ]
}
```
***
