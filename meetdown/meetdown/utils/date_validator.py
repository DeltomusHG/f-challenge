from datetime import datetime
from meetdown.meetings.models import Meetings
from django.db.models import Q


class DTValidator:
    def validate(self, validated_data):
        start_date = validated_data["start_date"]
        end_date = validated_data["end_date"]
        location_id = validated_data["location"].id
        if start_date > end_date:
            return False

        meetings = Meetings.objects.filter(
            Q(location_id=location_id),
            Q(start_date__range=(start_date, end_date))
            | Q(end_date__range=(start_date, end_date))
            | Q(start_date__lte=start_date, end_date__gte=end_date)
            | Q(start_date__gte=start_date, end_date__lte=end_date),
        )

        if meetings:
            return False
        return True
