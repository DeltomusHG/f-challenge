from rest_framework.routers import SimpleRouter
from meetdown.meetings import views

router = SimpleRouter()

router.register(r"", views.MeetingsViewSet)

urlpatterns = []

urlpatterns += router.urls
