from django.db import models
from meetdown.locations.models import Locations
from meetdown.users.models import Users
from simple_history.models import HistoricalRecords


class MeetingCategories(models.Model):
    name = models.CharField(max_length=50)


class MeetingTypes(models.Model):
    name = models.CharField(max_length=50)


class Meetings(models.Model):
    name = models.CharField(max_length=50)
    start_date = models.DateTimeField(unique=True)
    end_date = models.DateTimeField(unique=True)
    location = models.ForeignKey(Locations, on_delete=models.CASCADE)
    available_tickets = models.IntegerField()
    meet_type = models.ForeignKey(MeetingTypes, on_delete=models.PROTECT)
    organizer = models.ForeignKey(Users, on_delete=models.CASCADE)
    category = models.ForeignKey(MeetingCategories, on_delete=models.CASCADE)
    is_active = models.BooleanField()
    waiting_list_tickets = models.IntegerField()
    history = HistoricalRecords()

    # NOTE Pendiente por validar que el organizador sea admin de eventos
