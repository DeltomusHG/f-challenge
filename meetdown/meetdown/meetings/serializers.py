from rest_framework import serializers
from meetdown.meetings.models import Meetings
from meetdown.utils.date_validator import DTValidator


class MeetingsSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        dtv = DTValidator()
        if not dtv.validate(validated_data):
            raise serializers.ValidationError(
                "La fecha elegida se traslapa con otro evento"
            )
        return super().create(validated_data)

    class Meta:
        model = Meetings
        fields = "__all__"
