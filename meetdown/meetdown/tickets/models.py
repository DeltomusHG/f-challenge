from django.db import models
from meetdown.users.models import Users
from meetdown.meetings.models import Meetings
from meetdown.locations.models import Locations
from simple_history.models import HistoricalRecords


class TicketStatus(models.Model):
    name = models.CharField(max_length=50)


class Tickets(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    meeting = models.ForeignKey(Meetings, on_delete=models.CASCADE)
    location = models.ForeignKey(Locations, on_delete=models.CASCADE)
    ticket_status = models.ForeignKey(TicketStatus, on_delete=models.PROTECT)
    history = HistoricalRecords()
