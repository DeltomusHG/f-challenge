from rest_framework import serializers
from meetdown.tickets.models import Tickets, TicketStatus


class TicketsSerializer(serializers.ModelSerializer):
    ticket_status = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        meeting = validated_data["meeting"]
        if meeting.available_tickets == 0:
            meeting.waiting_list_tickets += 1
            pk = 2
        else:
            meeting.available_tickets -= 1
            pk = 1

        validated_data["ticket_status"] = TicketStatus.objects.get(pk=pk)
        meeting.save()

        return super().create(validated_data)

    class Meta:
        model = Tickets
        fields = "__all__"
