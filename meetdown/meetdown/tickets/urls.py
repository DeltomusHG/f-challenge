from rest_framework.routers import SimpleRouter
from meetdown.tickets import views

router = SimpleRouter()

router.register(r"", views.TicketsViewSet)

urlpatterns = []

urlpatterns += router.urls
