from rest_framework.routers import SimpleRouter
from meetdown.users import views


router = SimpleRouter()

router.register(r"", views.UsersViewSet)

urlpatterns = []

urlpatterns += router.urls
