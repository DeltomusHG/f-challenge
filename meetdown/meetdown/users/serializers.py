from rest_framework import serializers
from meetdown.users.models import Users
from django.contrib.auth.hashers import make_password


class UsersSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def validate_password(self, value):
        return make_password(value)

    class Meta:
        model = Users
        fields = "__all__"
