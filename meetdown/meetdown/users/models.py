from django.db import models
from django.contrib.auth.models import AbstractUser
from simple_history.models import HistoricalRecords


class UserType(models.Model):
    name = models.CharField(max_length=50)


class Users(AbstractUser):
    name = models.CharField(max_length=50)
    paternal_surname = models.CharField(max_length=50)
    maternal_surname = models.CharField(max_length=50)
    email = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=150)
    user_type = models.ForeignKey(UserType, on_delete=models.PROTECT)
    history = HistoricalRecords()

    REQUIRED_FIELDS = []
    USERNAME_FIELD = "email"
