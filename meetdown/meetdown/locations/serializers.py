from rest_framework import serializers
from meetdown.locations.models import Locations


class LocationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Locations
        fields = "__all__"
