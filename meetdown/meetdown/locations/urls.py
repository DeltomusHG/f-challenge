from rest_framework.routers import SimpleRouter
from meetdown.locations import views

router = SimpleRouter()

router.register(r"", views.LocationsViewSet)

urlpatterns = []

urlpatterns += router.urls
