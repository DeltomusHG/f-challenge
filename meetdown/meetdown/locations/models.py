from django.db import models
from simple_history.models import HistoricalRecords


class Locations(models.Model):
    name = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    neighboorhood = models.CharField(max_length=50)
    street_name = models.CharField(max_length=50)
    number = models.CharField(max_length=50)
    zip_code = models.IntegerField()
    capacity = models.IntegerField()
    history = HistoricalRecords()
